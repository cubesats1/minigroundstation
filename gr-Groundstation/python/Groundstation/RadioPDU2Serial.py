#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2023 rodrigo.munoz.l@ug.uchile.cl.
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
import struct
import numpy
from gnuradio import gr
import pmt
import serial
from . import crc32c

FEND = numpy.uint8(0xc0)
FESC = numpy.uint8(0xdb)
TFEND = numpy.uint8(0xdc)
TFESC = numpy.uint8(0xdd)

class RadioPDU2Serial(gr.basic_block):
    """
    docstring for block RadioPDU2Serial
    """
    def __init__(self, SerialPort):
        gr.basic_block.__init__(self,
            name="RadioPDU2Serial",
            in_sig=None,
            out_sig=None)
        self.port = SerialPort
        self.message_port_register_in(pmt.intern('in'))
        self.set_msg_handler(pmt.intern('in'), self.handle_msg)
        self.message_port_register_out(pmt.intern('out'))
        self.ser = serial.Serial(SerialPort)


    def handle_msg(self, msg_pmt):
        msg = pmt.cdr(msg_pmt)
        #print(zlib.crc32(bytearray(msg)))
        if not pmt.is_u8vector(msg):
            print('[ERROR] Received invalid message type. Expected u8vector')
            return
        packet = bytes(pmt.u8vector_elements(msg))
        if (packet[3] & 0x01) == 0x01:
            packet = packet[:-4]
            crc1 = crc32c.crc(packet[4:])
            packet += struct.pack('>I',crc1)
            print("crc enabled")
        crc2 = crc32c.crc(packet[4:])
        packet += struct.pack('>I',crc2)
        control = [numpy.uint8(0)]
        frame = ([FEND] + control
                 + self.kiss_escape(packet) 
                 + [FEND])
        self.ser.write(frame)
        self.message_port_pub(
            pmt.intern('out'),
            pmt.cons(pmt.PMT_NIL, pmt.init_u8vector(len(frame), frame)))

    def kiss_escape(self,a):
        """Escapes KISS control characters

        This replaces FEND and FESC according to the KISS escape rules
        """
        buff = list()
        c = 1
        for x in a:
            if x == FESC:
                buff.append(FESC)
                buff.append(TFESC)
            elif x == FEND:
                buff.append(FESC)
                buff.append(TFEND)
            else:
                buff.append(numpy.uint8(x))
            c = c +1
        return buff

