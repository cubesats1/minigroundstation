find_package(PkgConfig)

PKG_CHECK_MODULES(PC_GR_GROUNDSTATION gnuradio-Groundstation)

FIND_PATH(
    GR_GROUNDSTATION_INCLUDE_DIRS
    NAMES gnuradio/Groundstation/api.h
    HINTS $ENV{GROUNDSTATION_DIR}/include
        ${PC_GROUNDSTATION_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    GR_GROUNDSTATION_LIBRARIES
    NAMES gnuradio-Groundstation
    HINTS $ENV{GROUNDSTATION_DIR}/lib
        ${PC_GROUNDSTATION_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
          )

include("${CMAKE_CURRENT_LIST_DIR}/gnuradio-GroundstationTarget.cmake")

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(GR_GROUNDSTATION DEFAULT_MSG GR_GROUNDSTATION_LIBRARIES GR_GROUNDSTATION_INCLUDE_DIRS)
MARK_AS_ADVANCED(GR_GROUNDSTATION_LIBRARIES GR_GROUNDSTATION_INCLUDE_DIRS)
